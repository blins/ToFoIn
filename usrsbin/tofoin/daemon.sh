#!/usr/local/bin/bash
# by LordNicky v1.0 20170713
. /usr/local/etc/tofoin.conf

timer_function () {
if [ "`expr $current_time - $test_time`" -ge "$TESTERPERIOD" ]
then 
for ((a=0; a < CNUMBER; a++))
do
  eval timeout "$TESTERLIMIT"s $TESTER '$RTABLE_'$a 10 $a & 
done
test_time=`date +%s`;
else :;
fi
if [ "`expr $current_time - $judge_time`" -ge "$JUDGEPERIOD" ]
then timeout "$JUDGELIMIT"s $JUDGE & judge_time=`date +%s`;
else :;
fi
}	

scout_function () {
if [ "`expr $current_time - $scout_time`" -ge "$SCOUTPERIOD" ]
then timeout "$SCOUTLIMIT"s $SCOUT & scout_time=`date +%s`;
else :;
fi
}	

carpstate_function () {
for ((a=0; a < 2 ; a++))
do
if ifconfig $INT_IF | grep carp | egrep -q MASTER;	
then if ifconfig | grep carp | egrep -q BACKUP;
     then a=0;
          while [ $a -lt ${#allifarea[@]} ] 
          do
            sudo ifconfig ${allifarea[$a]} vhid $CARP_VHID state master;
            a=`expr $a + 1`
          done
     timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: All CARP interfaces to Master" &
     else :;
     fi
carpstate=0; break
elif ifconfig $INT_IF | grep carp | egrep -q BACKUP;
then if ifconfig | grep carp | egrep -q MASTER;
     then a=0;
          while [ $a -lt ${#allifarea[@]}  ] 
          do
            sudo ifconfig ${allifarea[$a]} vhid $CARP_VHID state backup;
            a=`expr $a + 1`
          done
     timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: All CARP interfaces to Backup" &
     else :;
     fi
carpstate=1; break
else :;
fi
if [ "$a" -eq "1" ]
then timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: No MASTER or BACKUP information about carp state." & rm $DAEMON_PID; exit 1;
else :;
fi	
done
}

defaultoptions_function () {
cp $FIRESET_0 $FIRESETDEF;
cp $BINDSET_0 $BINDSETDEF;
sudo /etc/rc.d/ipfw restart;
sudo /usr/local/etc/rc.d/named restart;
}	

if [ -e $DAEMON_PID ]
then $WATCHDOG "daemon" & exit 0;
else test_time=`date +%s`;
judge_time=`date +%s`;
scout_time=`date +%s`;

echo `date +%s` > $DAEMON_PID;
echo $$ >> $DAEMON_PID;
timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: Start successfully with pid $$" &

allifarea=( $ALL_IF )
carpstate_function;
echo "$carpstate" > $PREVSTATE;

     if [ "$carpstate" -eq "0" ]
     then setfib $RTABLE_0 sudo route del default;
     setfib $RTABLE_0 sudo route add default $DEFAULT_GATEWAY;
     for ((a=1; a < CNUMBER ; a++))
     do
       sudo /usr/local/etc/rc.d/setfib$a start;
     done
          if [ -z $ADDITLAN ]
          then :;
          else sudo /usr/local/etc/rc.d/setfib$ADDITLAN start;
          fi	 
     defaultoptions_function;
     for ((a=0; a < CNUMBER ; a++))
     do
       eval timeout "$TESTERLIMIT"s $TESTER '$RTABLE_'$a 10 $a & 
     done
     elif [ "$carpstate" -eq "1" ]
     then timeout "$SCOUTLIMIT"s $SCOUT & scout_time=`date +%s`;
     else timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: Problem with carpstate value. Exiting" & rm $DAEMON_PID; exit 1;
     fi

     while true
     do
       current_time=`date +%s`;
       carpstate_function;
       if [ "$carpstate" -eq "0" ]
       then prevstate=`cat $PREVSTATE`;
            if [ "$prevstate" -eq "0" ]
            then timer_function;
            elif [ "$prevstate" -eq "1" ]
            then setfib $RTABLE_0 sudo route del default;
            setfib $RTABLE_0 sudo route add default $DEFAULT_GATEWAY;
	    for ((a=1; a < CNUMBER ; a++))
            do
              sudo /usr/local/etc/rc.d/setfib$a start;
            done
	         if [ -z $ADDITLAN ]
	         then :;
		 else sudo /usr/local/etc/rc.d/setfib$ADDITLAN start;
		 fi	 
            defaultoptions_function;
            judge_time=`date +%s`;
            timer_function;
            else timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: Strange previous state information: $prevstate" &
            fi
       elif [ "$carpstate" -eq "1" ]
       then scout_function;
            if [ -e $SCOUTGATE ];
            then workgateway=`sed -n 2p $SCOUTGATE`;
            else :;
            fi
            if [ -z $workgateway ]
            then :;
            else if netstat -r -n | grep default | egrep -q $workgateway;
 	         then :;
                 else setfib $RTABLE_0 sudo route del default;
                 setfib $RTABLE_0 sudo route add default $workgateway;
	         timeout "$LOGGERLIMIT"s $LOGGER "DAEMON: Switching gateway to $workgateway" &
                 fi
            fi
       fi
       echo "$carpstate" > $PREVSTATE;
       sleep $SENSITIVITY;
     done
fi    
