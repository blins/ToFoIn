#!/usr/local/bin/bash
# by LordNicky v1.0 20170619
. /usr/local/etc/tofoin.conf

exit_function () {
rm $tester_pid; 
exit $exit_code;
}

tester_pid=$TESTER_PID/tofoin_test_$3\.pid;
if [ -e $tester_pid ];
then timeout "$WATCHDOGLIMIT"s $WATCHDOG "tester" "$tester_pid" "$3" & exit 0;
else echo `date +%s` > $tester_pid;
echo $$ >> $tester_pid;
timelimit=`expr $PNUMBER \* 10`;	
     if [ "$2" -eq 10 ];
     then for ((a=0; a < $TNUMBER; a++ ))
          do 
          if eval timeout "$timelimit"s setfib $1 ping -c $PNUMBER '$PTARGET_'$a > /dev/null;
          then echo `date +%s` > $DIR_TMP/result_$3;
          echo "0" >> $DIR_TMP/result_$3;
          echo "$a" >> $DIR_TMP/result_$3;
          exit_code=0; exit_function;
          else :;
          fi
          done 
     echo `date +%s` > $DIR_TMP/result_$3;
     echo "1" >> $DIR_TMP/result_$3;
     echo "1" >> $DIR_TMP/result_$3;
     exit_code=0; exit_function;
     elif [ "$2" -eq 0 ];
     then setfib $1 ping -c $PNUMBER $PTARGET_0;
     exit_code=0; exit_function;
     elif [ "$2" -eq 1 ];
     then setfib $1 ping -c $PNUMBER $PTARGET_1;
     exit_code=0; exit_function;
     else setfib $1 ping -c $PNUMBER $2;
     exit_code=1; exit_function;
     fi
fi     

