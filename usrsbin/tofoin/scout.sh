#!/usr/local/bin/bash
# by LordNicky v1.0 20170713
. /usr/local/etc/tofoin.conf

exit_function () {
rm $SCOUT_PID; 
exit $exit_code;
}

ping_function () {
for ((b=0; b < TNUMBER; b++))
do
  if eval setfib $RTABLE_0 ping -c $PNUMBER '$PTARGET_'$b > /dev/null;
  then return 0;
  else return 1; 	   
  fi
done
}
	
yesaccess_function () {
for ((a=1; a < RNUMBER; a++))
do 
  if netstat -r -n | grep default | eval egrep -q '$ASERV_IP_'$a;
  then eval workgateway='$ASERV_IP_'$a; break
  else continue
  fi	  
done
if [ -z $workgateway ]
then noaccess_function;
else echo `date +%s` > $SCOUTGATE;
echo $workgateway >> $SCOUTGATE; exit_code=0; exit_function;
fi
}

noaccess_function () {
for ((a=1; a < RNUMBER; a++))
do	
  if ifconfig $INT_IF | grep carp | egrep -q MASTER;
  then exit_code=0; exit_function;
  elif ifconfig $INT_IF | grep carp | egrep -q BACKUP;
  then setfib $RTABLE_0 sudo route del default;
  eval setfib $RTABLE_0 sudo route add default '$ASERV_IP_'$a;
  ping_function; return_val=$?;
       if [ "$return_val" -eq 0 ]
       then echo `date +%s` > $SCOUTGATE;
       eval echo '$ASERV_IP_'$a >> $SCOUTGATE; exit_code=0; exit_function;  
       elif [ "$return_val" -eq 1 ] 
       then continue
       else $LOGGER "SCOUT: Strange return_val= $return_val"; continue
       fi
  else exit_code=0; exit_function;     
  fi     
done
}	

if [ -e $SCOUT_PID ];
then timeout "$WATCHDOGLIMIT"s $WATCHDOG "scout" & exit 0;
else echo `date +%s` > $SCOUT_PID;
     echo $$ >> $SCOUT_PID;
ping_function; return_val=$?;
     if [ "$return_val" -eq 0 ]
     then yesaccess_function;
     elif [ "$return_val" -eq 1 ] 
     then noaccess_function;
     else $LOGGER "SCOUT: Strange return_val= $return_val";
     fi
fi 

exit_code=0; exit_function;
