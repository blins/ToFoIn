#!/usr/local/bin/bash
# by LordNicky v1.0 20170619
. /usr/local/etc/tofoin.conf

exit_function () {
rm $JUDGE_PID; 
exit $exit_code;
}

decision_function () {
if [ "$actualchan" -eq "$prefchan" ];
then if [ "$actualchan" -eq 0 ];
     then timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: No problems detected" & 
     exit_code=0; exit_function;
     elif [ "$actualchan" -gt 0 ];
     then if [ "$actualchan" -lt "$CNUMBER" ];
          then echo -e "0" > $JUDGEMETER; 
          timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: No problems detected at channel $actualchan" & 
          exit_code=0; exit_function;
          else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(decision): Invalid actualchan = $actualchan" & 
          exit_code=1; exit_function;
          fi
     else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(decision): Invalid actualchan = $actualchan" & 
     exit_code=1; exit_function;
     fi
else if [ "$prefchan" -eq 0 ];
     then if [ "$actualstate" -eq 0 ];
          then meter=`cat $JUDGEMETER`;
               if [ "$meter" -eq "$WNUMBER" ];
	       then switch_function; exit_code=0; exit_function;
	       elif [ "$meter" -lt "$WNUMBER" ];
	       then expr $meter + 1 > $JUDGEMETER; 
	       exit_code=0; exit_function;
	       else echo -e "0" > $JUDGEMETER; exit_code=0; exit_function;
	       fi
	  elif [ "$actualstate" -eq 1 ]
	  then timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: Emergency switch to $prefchan" & 
	  switch_function; exit_code=0; exit_function;
	  else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(decision): Invalid actualstate = $actualstate" & exit_code=1; exit_function;
	  fi     	
     elif [ "$prefchan" -gt 0 ];
     then if [ "$prefchan" -lt "$CNUMBER" ];
          then switch_function; exit_code=0; exit_function;
          else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(decision): Invalid prefchan = $prefchan" & 
          exit_code=1; exit_function;
          fi	   	 
     else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(decision): Invalid prefchan = $prefchan" & 
     exit_code=1; exit_function;
     fi	   	 
fi
} 

switch_function () {
echo -e "0" > $JUDGEMETER;
if [ -z $prefchan ]
then timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(switch): Invalid prefchan = $prefchan" & 
exit_code=1; exit_function;
else sudo /usr/local/etc/rc.d/named stop; 
eval cp '$FIRESET_'$prefchan $FIRESETDEF;
eval cp '$BINDSET_'$prefchan $BINDSETDEF;
sudo /etc/rc.d/ipfw restart; 
eval setfib '$RTABLE_'$prefchan sudo /usr/local/etc/rc.d/named start; 
eval timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: Now switching on channel '$RTABLE_'$prefchan" & 
exit_code=0; exit_function;
fi
}	

createarea_function () {
for ((a=0; a < CNUMBER ; a++))
do
  current_time=`date +%s`
  timearea[$a]=`sed -n 1p $DIR_TMP/result_$a`;
  if [ "`expr $current_time - ${timearea[$a]}`" -ge 0 ];
  then if [ "`expr $current_time - ${timearea[$a]}`" -lt "`expr $TESTERPERIOD + 120`" ];
       then :;
       else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(createarea): MAX period" & 
       timeout "$WATCHDOGLIMIT"s $WATCHDOG & 
       exit_code=1; exit_function;
       fi
  else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(createarea): testmodule $a in future" & 
  timeout "$WATCHDOGLIMIT"s $WATCHDOG & 
  exit_code=1; exit_function;
  fi	
  statearea[$a]=`sed -n 2p $DIR_TMP/result_$a`;
  if [ "$actualchan" -eq "$a" ]
  then actualstate=${statearea[$a]};
  else continue   
  fi	  
done
}

findarea_function () {
for ((a=0; a < CNUMBER ; a++))
do
  if [ "${statearea[$a]}" -eq 0 ];
  then prefchan=$a; decision_function; 
  exit_code=0; exit_function; 
  else if [ "${statearea[$a]}" -eq 1 ]
       then continue 
       else timeout "$LOGGERLIMIT"s $LOGGER "JUDGE(findarea): Invalid channel state for a=$a" & 
       exit_code=1; exit_function;
       fi  
  fi
done
}

if [ -e $JUDGE_PID ];
then timeout "$WATCHDOGLIMIT"s $WATCHDOG "judge" & exit 0;
else echo `date +%s` > $JUDGE_PID;	
     echo $$ >> $JUDGE_PID;	
     for ((a=0; a < CNUMBER ; a++))
     do
       if sed -n 1p $FIRESETDEF | egrep -q $a;
       then actualchan=$a;
       else :;
       fi
     done
     if [ -z $actualchan ];
     then timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: Actualchan is empty" &
     prefchan=0;
     switch_function;
     exit_code=1; exit_function;
     else createarea_function;
     findarea_function;
     timeout "$LOGGERLIMIT"s $LOGGER "JUDGE: All channels down" & 
     exit_code=1; exit_function;
     fi
fi     
