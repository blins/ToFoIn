#!/usr/local/bin/bash
# by LordNicky v1.0 20170717
. /usr/local/etc/tofoin.conf

exit_function () {
rm $WATCHDOG_PID; 
exit $exit_code;
}

kill_function () {
if [[ "`ps -o command -p $proc_pid | grep "tofoin" | grep -o "$proc_name"`" = "$proc_name" ]];
then timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Other $proc_name working during $diff, kill him" &
sudo kill $proc_pid; 
else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: None or other process on $proc_s_name pid, cleaning pid file" &
fi
if [[ "$proc_name" = "watchdog" ]];
then main_function;
else rm $proc_pid_file;
fi
}	

main_function () {
echo `date +%s` > $WATCHDOG_PID;
echo $$ >> $WATCHDOG_PID;
proc_name=${rec_name:-all};
return_wait=10
if [[ "$proc_name" = "all" ]];
then if ifconfig $INT_IF | grep carp | egrep -q MASTER; 
     then for ((a=0; a < CNUMBER ; a++))
          do 
            current_time=`date +%s`;
            tester_time_tmp=`sed -n 1p $DIR_TMP/result_$a`;
            tester_time=${tester_time_tmp:-0};
            diff=`expr $current_time - $tester_time`;
            if [ "$diff" -ge 0 ]
            then if [ "$diff" -lt "`expr $TESTERPERIOD + 120`" ];
                 then :;
                 else killemall_function;
                 exit_code=0; exit_function;
                 fi
            else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Check date" &
            killemall_function;
            exit_code=0; exit_function;
            fi
          done
     elif ifconfig $INT_IF | grep carp | egrep -q BACKUP;
     then current_time=`date +%s`;
          scout_time_tmp=`sed -n 1p $SCOUTGATE`;
          scout_time=${scout_time_tmp:-0};
          diff=`expr $current_time - $scout_time`;
          if [ "$diff" -ge 0 ]
          then if [ "$diff" -lt "`expr $SCOUTPERIOD + 120`" ];
               then :;
               else sleep 120;
                    if [ "$diff" -lt "`expr $SCOUTPERIOD + 120`" ];
                    then :;
                    else killemall_function;
                    exit_code=0; exit_function;
                    fi
	       fi     
          else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Check date" &
          killemall_function;
          exit_code=0; exit_function;
          fi
     else sudo ifconfig $INT_IF down;
     sudo ifconfig $INT_IF up;
     timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Unknown carp state" &
     fi 
elif [[ "$proc_name" = "daemon" ]];
then proc_s_name=$proc_name;
     for ((a=0; a < CNUMBER ; a++))
     do 
       current_time=`date +%s`;
       tester_time_tmp=`sed -n 1p $DIR_TMP/result_$a`;
       tester_time=${tester_time_tmp:-0};
       diff=`expr $current_time - $tester_time`;
       if [ "$diff" -ge 0 ]
       then if [ "$diff" -lt "`expr $TESTERPERIOD + 120`" ];
            then :;
            else /usr/local/etc/rc.d/tofoin restart;
            exit_code=0; exit_function;
            fi
       else timeout "$WATCHDOGLIMIT"s $LOGGER "WATCHDOG: Check date" &
       /usr/local/etc/rc.d/tofoin restart;
       exit_code=0; exit_function;
       fi
     done
     if ps -ax | grep "[t]ofoin/daemon"
     then :;
     else /usr/local/etc/rc.d/tofoin restart;
     fi
elif [[ "$proc_name" = "tester" ]];
then proc_pid_file=$rec_pid; cnumber=$rec_num; limit=$TESTERLIMIT; proc_s_name="$proc_name $cnumber channel";
module_function; return_val=$?;
     if [[ "$return_val" = "$return_wait" ]];
     then sleep $TESTERLIMIT; module_function "nowait";
     else :;
     fi
elif [[ "$proc_name" = "judge" ]];
then proc_pid_file=$JUDGE_PID; limit=$JUDGELIMIT; proc_s_name=$proc_name;
module_function; return_val=$?;
     if [[ "$return_val" = "$return_wait" ]];
     then sleep $JUDGELIMIT; module_function "nowait";
     else :;
     fi
elif [[ "$proc_name" = "logger" ]];
then proc_pid_file=$LOGGER_PID; limit=$LOGGERLIMIT; proc_s_name=$proc_name;
module_function; return_val=$?;
     if [[ "$return_val" = "$return_wait" ]];
     then sleep $LOGGERLIMIT; module_function "nowait";
     else :;
     fi
     echo -e "`date -j +%Y%m%d%H%M` WATCHDOG: Logger was killed now." >> $LOGFILE;
else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Incorrect process name";
fi
exit_code=0; exit_function;
}	

killemall_function () {
proc_name="daemon"; proc_s_name=$proc_name; proc_pid=`sed -n 2p $DAEMON_PID`; kill_function;
proc_name="logger"; proc_s_name=$proc_name; proc_pid=`sed -n 2p $LOGGER_PID`; kill_function;
proc_name="judge"; proc_s_name=$proc_name; proc_pid=`sed -n 2p $JUDGE_PID`; kill_function;
proc_name="scout"; proc_s_name=$proc_name; proc_pid=`sed -n 2p $SCOUT_PID`; kill_function;
/usr/local/etc/rc.d/tofoin restart;
}

module_function () {
if [ -e $proc_pid_file ];
then proc_pid=`sed -n 2p $proc_pid_file`;
     start_time=`sed -n 1p $proc_pid_file`;
     current_time=`date +%s`;
     diff=`expr $current_time - $start_time`;
     if [ "$diff" -ge 0 ];
     then if [ "$diff" -lt "$limit" ];
          then if [[ "$1" = "nowait" ]];
	       then if [ "$proc_pid" = "$proc_temp_pid" ];
	            then kill_function; return 0;
		    else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Pid of $proc_s_name was changed, exit" &
		    fi
	       else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: $proc_s_name now working, try wait" &
	       proc_temp_pid=$proc_pid;
	       return $return_wait;
	       fi
	  else kill_function; return 0;
	  fi
     else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Time error in $proc_s_name = $diff" &
     kill_function; return 0;
     fi 
else return 0;
fi
}	

rec_name=$1;
rec_pid=$2;
rec_num=$3;

if [ -e $WATCHDOG_PID ];
then proc_pid=`sed -n 2p $WATCHDOG_PID`;
     proc_name="watchdog";
     other_start_time=`sed -n 1p $WATCHDOG_PID`;
     current_time=`date +%s`;
     diff=`expr $current_time - $other_start_time`;
     if [ "$diff" -ge 0 ];
     then if [ "$diff" -lt "`expr $TESTERLIMIT + $JUDGELIMIT + $LOGGERLIMIT + 30`" ];
          then timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Other $proc_s_name already working, exit" & exit 0;
	  else kill_function;
	  fi
     else timeout "$LOGGERLIMIT"s $LOGGER "WATCHDOG: Time error in $proc_s_name = $diff" &
     kill_function; 
     fi 
else echo `date +%s` > $WATCHDOG_PID;
echo $$ >> $WATCHDOG_PID;	
main_function;
fi
