#!/usr/local/bin/bash
# by LordNicky v1.0 20170706

echo "Now I will check and create if need user and group.";

if pw usershow tofoin > /dev/null;
then :;
else pw groupadd -n tofoin -g 1500;
pw useradd -n tofoin -u 1500 -G tofoin -d /nonexistent -s /bin/sh -c "Toogle Failover of Internet";
fi

echo "Now I will copy ToFoIn files to they places and create some servicve files, if it needed.";

usrsbin_function () {
mkdir /usr/local/sbin/tofoin;
cp ./usrsbin/tofoin/daemon.sh /usr/local/sbin/tofoin/;
cp ./usrsbin/tofoin/judge.sh /usr/local/sbin/tofoin/;
cp ./usrsbin/tofoin/logger.sh /usr/local/sbin/tofoin/;
cp ./usrsbin/tofoin/tester.sh /usr/local/sbin/tofoin/;
cp ./usrsbin/tofoin/scout.sh /usr/local/sbin/tofoin/;
cp ./usrsbin/tofoin/watchdog.sh /usr/local/sbin/tofoin/;
}	

if [ -e /usr/local/sbin/tofoin ];
then rm -r /usr/local/sbin/tofoin; usrsbin_function;
else usrsbin_function;
fi

if [ -e /usr/local/etc/tofoin.conf ];
then cp ./usretc/tofoin.conf /usr/local/etc/tofoin.conf.default;
else cp ./usretc/tofoin.conf /usr/local/etc/;
fi

cp ./usretc/tofoin /usr/local/etc/rc.d/;
cp ./usretc/sudoers.d/tofoin /usr/local/etc/sudoers.d/;

echo "Now I will set necessary file permissions."
chown tofoin /usr/local/etc/namedb/named.conf*
chown tofoin /etc/firewall/config*

